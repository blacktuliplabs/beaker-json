from beaker.container import NamespaceManager, Container
from beaker.exceptions import InvalidCacheBackendError, MissingCacheParameter
from beaker.synchronization import null_synchronizer
from beaker.util import SyncDict

try:
    from pymongo.connection import Connection
    import bson
    import bson.errors
except ImportError:
    raise InvalidCacheBackendError("Unable to load the pymongo driver.")


class MongoDBNamespaceManager(NamespaceManager):
    clients = SyncDict()
    _pickle = True
    _sparse = False

    # TODO _- support write concern / safe
    def __init__(self, namespace, url=None, data_dir=None, skip_pickle=False, sparse_collection=False, **params):
        NamespaceManager.__init__(self, namespace)

        if not url:
            raise MissingCacheParameter("MongoDB url is required")

        if skip_pickle:
            self._pickle = False

        if sparse_collection:
            self._sparse = True

        # Temporarily uses a local copy of the functions until pymongo upgrades to new parser code
        (host_list, database, username, password, collection, options) = _parse_uri(url)

        if database and host_list:
            data_key = "mongodb:%s" % (database)
        else:
            raise MissingCacheParameter("Invalid Cache URL.  Cannot parse.")

        def _create_mongo_conn():
            host_uri = 'mongodb://'
            for x in host_list:
                host_uri += '%s:%s' % x
            conn = Connection(host_uri, slave_okay=options.get('slaveok', False))

            db = conn[database]

            if username:
                if not db.authenticate(username, password):
                    raise InvalidCacheBackendError('Cannot authenticate to '
                                                   ' MongoDB.')
            return db[collection]

        self.mongo = MongoDBNamespaceManager.clients.get(data_key, _create_mongo_conn)

    def get_creation_lock(self, key):
        """@TODO - stop hitting filesystem for this...
        I think mongo can properly avoid dog piling for us.
        """
        return null_synchronizer

    def do_remove(self):
        """Clears the entire filesystem (drops the collection)"""
        q = {}
        if self._sparse:
            q = {'_id.namespace': self.namespace}
        else:
            q = {'_id': self.namespace}

        self.mongo.remove(q)

    def __getitem__(self, key):
        _id = {}
        fields = {}
        if self._sparse:
            _id = {
                'namespace': self.namespace,
                'key': key
            }
            fields['data'] = True
        else:
            _id = self.namespace
            fields['data.' + key] = True

        result = self.mongo.find_one({'_id': _id}, fields=fields)

        if result:
            """Running into instances in which mongo is returning
            -1, which causes an error as __len__ should return 0
            or positive integers, hence the check of size explicit"""
            data = result.get('data', None)
            if self._sparse:
                value = data
            else:
                value = data.get(key, None)

            if not value:
                return None

            if self._pickle or key == 'session':
                value = value
            else:
                value = (value['stored'], value['expires'], value['value'])

            return value
        else:
            return None

    def __contains__(self, key):
        def _has():
            result = self.__getitem__(key)
            if result:
                return result is not None
            else:
                return False

        ret = _has()
        return ret

    def has_key(self, key):
        return key in self

    def set_value(self, key, value, expiretime=None):
        _id = {}
        doc = {}

        if self._pickle or key == 'session':
            value = value
        else:
            value = {
                'stored': value[0],
                'expires': value[1],
                'value': value[2],
                'pickled': False
            }
            try:
                bson.encode(value)
            except:
                value['value'] = value['value']
                value['pickled'] = True

        if self._sparse:
            _id = {
                'namespace': self.namespace,
                'key': key
            }

            doc['data'] = value
            doc['_id'] = _id
            if expiretime:
                # TODO - What is the datatype of this? it should be instantiated as a datetime instance
                doc['valid_until'] = expiretime
        else:
            _id = self.namespace
            doc['$set'] = {'data.' + key: value}
            if expiretime:
                # TODO - What is the datatype of this? it should be instantiated as a datetime instance
                doc['$set']['valid_until'] = expiretime

        self.mongo.update({"_id": _id}, doc, upsert=True, safe=True)

    def __setitem__(self, key, value):
        self.set_value(key, value)

    def __delitem__(self, key):
        """Delete JUST the key, by setting it to None."""
        if self._sparse:
            self.mongo.remove({'_id.namespace': self.namespace})
        else:
            self.mongo.update({'_id': self.namespace},
                              {'$unset': {'data.' + key: True}}, upsert=False)

    def keys(self):
        if self._sparse:
            return [row['_id']['field'] for row in self.mongo.find({'_id.namespace': self.namespace}, {'_id': True})]
        else:
            return self.mongo.find_one({'_id': self.namespace}, {'data': True}).get('data', {})


class MongoDBContainer(Container):
    namespace_class = MongoDBNamespaceManager


def _partition(source, sub):
    """Our own string partitioning method.

    Splits `source` on `sub`.
    """
    i = source.find(sub)
    if i == -1:
        return (source, None)
    return (source[:i], source[i + len(sub):])


def _str_to_node(string, default_port=27017):
    """Convert a string to a node tuple.

    "localhost:27017" -> ("localhost", 27017)
    """
    (host, port) = _partition(string, ":")
    if port:
        port = int(port)
    else:
        port = default_port
    return (host, port)


def _parse_uri(uri, default_port=27017):
    """MongoDB URI parser.
    """

    if uri.startswith("mongodb://"):
        uri = uri[len("mongodb://"):]
    elif "://" in uri:
        raise InvalidURI("Invalid uri scheme: %s" % _partition(uri, "://")[0])

    (hosts, namespace) = _partition(uri, "/")

    raw_options = None
    if namespace:
        (namespace, raw_options) = _partition(namespace, "?")
        if '.' not in namespace and '#' not in namespace:
            db = namespace
            collection = None
        else:
            if '#' in namespace:
                (db, collection) = namespace.split("#", 1)
            else:
                (db, collection) = namespace.split(".", 1)
    else:
        db = None
        collection = None

    username = None
    password = None
    if "@" in hosts:
        (auth, hosts) = _partition(hosts, "@")

        if ":" not in auth:
            raise InvalidURI("auth must be specified as "
                             "'username:password@'")
        (username, password) = _partition(auth, ":")

    host_list = []
    for host in hosts.split(","):
        if not host:
            raise InvalidURI("empty host (or extra comma in host list)")
        host_list.append(_str_to_node(host, default_port))

    options = {}
    if raw_options:
        and_idx = raw_options.find("&")
        semi_idx = raw_options.find(";")
        if and_idx >= 0 and semi_idx >= 0:
            raise InvalidURI("Cannot mix & and ; for option separators.")
        elif and_idx >= 0:
            options = dict([kv.split("=") for kv in raw_options.split("&")])
        elif semi_idx >= 0:
            options = dict([kv.split("=") for kv in raw_options.split(";")])
        elif raw_options.find("="):
            options = dict([raw_options.split("=")])

    return (host_list, db, username, password, collection, options)
